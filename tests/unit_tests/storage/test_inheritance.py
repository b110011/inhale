from inhale.storage import Storage, InheritanceType


def test_add_base() -> None:
    storage = Storage()
    inheritance = storage.inheritance

    struct_data = storage.add_compound("Data", "structExample_1_1Data", "struct")
    struct_data_impl = storage.add_compound("DataImpl", "structExample_1_1Impl_1_1DataImpl", "struct")

    inheritance.add_base(struct_data_impl, struct_data, InheritanceType.PUBLIC)

    assert inheritance.get_bases(struct_data) == set()
    assert inheritance.get_bases(struct_data_impl) == set([(struct_data, InheritanceType.PUBLIC)])

    assert inheritance.get_derivatives(struct_data) == set()
    assert inheritance.get_derivatives(struct_data_impl) == set()


def test_add_str_base() -> None:
    storage = Storage()
    inheritance = storage.inheritance

    base = "boost::noncopyable"
    struct_data = storage.add_compound("Data", "structExample_1_1Data", "struct")

    inheritance.add_base(struct_data, base, InheritanceType.PUBLIC)

    assert inheritance.get_bases(struct_data) == set([(base, InheritanceType.PUBLIC)])
    assert inheritance.get_derivatives(struct_data) == set()


def test_add_derived() -> None:
    storage = Storage()
    inheritance = storage.inheritance

    struct_data = storage.add_compound("Data", "structExample_1_1Data", "struct")
    struct_data_impl = storage.add_compound("DataImpl", "structExample_1_1Impl_1_1DataImpl", "struct")

    inheritance.add_derived(struct_data, struct_data_impl, InheritanceType.PUBLIC)

    assert inheritance.get_bases(struct_data) == set()
    assert inheritance.get_bases(struct_data_impl) == set()

    assert inheritance.get_derivatives(struct_data) == set([(struct_data_impl, InheritanceType.PUBLIC)])
    assert inheritance.get_derivatives(struct_data_impl) == set()
