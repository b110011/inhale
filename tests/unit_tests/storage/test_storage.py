from inhale.storage import Storage, compounds

import pytest


def test_unsupported_compound() -> None:
    with pytest.raises(RuntimeError):
        Storage().add_compound("utils", "dir_0001", "directory")


def test_no_compound() -> None:
    with pytest.raises(KeyError):
        assert Storage().get_compound("test_refid")

    with pytest.raises(KeyError):
        storage = Storage()
        storage.add_compound("Example", "namespaceExample", "namespace")
        assert Storage().get_compound("test_refid")


def test_common() -> None:
    storage = Storage()

    ns = storage.add_compound("Example", "namespaceExample", "namespace")
    struct = storage.add_compound("Data", "structExample_1_1Data", "struct")

    assert storage.get_compound("namespaceExample") == ns
    assert storage.get_compound("structExample_1_1Data") == struct

    assert storage.get_compounds(compounds.Namespace) == [ns]
    assert storage.get_compounds(compounds.Struct) == [struct]


def test_name_collision() -> None:
    storage = Storage()

    s1 = storage.add_compound("Data", "structExample_1_1Data", "struct")
    s2 = storage.add_compound("Data", "structExample_1_1Impl_1_1Data", "struct")

    assert storage.has_name_collision(s1)
    assert storage.has_name_collision(s2)


def test_name_collision_with_missing_compound() -> None:
    with pytest.raises(KeyError):
        assert Storage().has_name_collision(compounds.Typedef(name="test", refid="test_refid"))
