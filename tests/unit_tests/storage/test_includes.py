from inhale.storage import Storage, compounds

_INCLUDES_EMPTY_RESULT = ((), ())


def test_add_include() -> None:
    storage = Storage()
    includes = storage.files

    sys_include = "cmath"

    data_hpp: compounds.File = storage.add_compound("data.hpp", "data_8hpp", "file")
    data_hpp.path = "example/data.hpp"

    data_impl_hpp: compounds.File = storage.add_compound("data_impl.hpp", "data_impl8hpp", "file")
    data_impl_hpp.path = "example/impl/data_impl.hpp"

    includes.add_include(data_impl_hpp, sys_include)
    includes.add_include(data_impl_hpp, data_hpp)

    assert includes.get_includes(data_hpp) == _INCLUDES_EMPTY_RESULT
    assert includes.get_includes(data_impl_hpp) == (set([data_hpp]), set(["cmath"]))

    assert includes.get_includers(data_hpp) == set()
    assert includes.get_includers(data_impl_hpp) == set()


def test_add_includer() -> None:
    storage = Storage()
    includes = storage.files

    data_hpp: compounds.File = storage.add_compound("data.hpp", "data_8hpp", "file")
    data_hpp.path = "example/data.hpp"

    data_impl_hpp: compounds.File = storage.add_compound("data_impl.hpp", "data_impl8hpp", "file")
    data_impl_hpp.path = "example/impl/data_impl.hpp"

    includes.add_includer(data_hpp, data_impl_hpp)

    assert includes.get_includes(data_hpp) == _INCLUDES_EMPTY_RESULT
    assert includes.get_includes(data_impl_hpp) == _INCLUDES_EMPTY_RESULT

    assert includes.get_includers(data_hpp) == set([data_impl_hpp])
    assert includes.get_includers(data_impl_hpp) == set()

