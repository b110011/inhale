from typing import Iterable, Generator, Type

from inhale.storage import Storage, compounds
from inhale.storage.compounds import BaseCompound


def _filter_compounds(compound_cls: Type[BaseCompound], it: Iterable[BaseCompound]) -> Generator[Iterable, None, None]:
    yield from filter(lambda c: c is not compound_cls, it)


def test_common() -> None:
    storage = Storage()
    relations = storage.relations

    ns = storage.add_compound("Impl", "namespaceImpl", "namespace")
    struct = storage.add_compound("Data", "structImpl_1_1Data", "struct")

    relations.add(ns, struct)

    # namespace

    assert not relations.has_parents(ns)
    assert relations.has_children(ns)

    for compound_cls in compounds.get_all():
        assert relations.get_parents(ns, compound_cls) == ()

    assert relations.get_children(ns, compounds.Struct) == [struct]
    for compound_cls in _filter_compounds(compounds.Struct, compounds.get_all()):
        assert relations.get_children(ns, compound_cls) == ()

    # struct

    assert relations.has_parents(struct)
    assert not relations.has_children(struct)

    assert relations.get_parents(struct, compounds.Namespace) == [ns]
    for compound_cls in _filter_compounds(compounds.Namespace, compounds.get_all()):
        assert relations.get_parents(struct, compound_cls) == ()

    for compound_cls in compounds.get_all():
        assert relations.get_children(struct, compound_cls) == ()


def test_get_fully_qualified_path() -> None:
    storage = Storage()
    relations = storage.relations

    """
    /data.hpp  <- Example::Data
    /impl/data_impl.hpp  <- Example::Impl::DataImpl
                            <- Example::Impl::DataImpl::Node
    """

    ns_example = storage.add_compound("Example", "namespaceExample", "namespace")
    ns_impl = storage.add_compound("Impl", "namespaceExample_1_1Impl", "namespace")
    relations.add(ns_example, ns_impl)

    data_hpp = storage.add_compound("data.hpp", "data_8hpp", "file")
    struct_data = storage.add_compound("Data", "structExample_1_1Data", "struct")
    relations.add(ns_example, struct_data)
    relations.add(data_hpp, struct_data)

    dir_impl = storage.add_compound("impl", "dir_0001", "dir")
    data_impl_hpp = storage.add_compound("data_impl.hpp", "data_impl8hpp", "file")
    relations.add(dir_impl, data_impl_hpp)

    assert relations.get_fully_qualified_path(struct_data) == "Example::Data"

    struct_data_impl = storage.add_compound("DataImpl", "structExample_1_1Impl_1_1DataImpl", "struct")
    relations.add(ns_impl, struct_data_impl)
    relations.add(data_impl_hpp, struct_data_impl)

    assert relations.get_fully_qualified_path(struct_data_impl) == "Example::Impl::DataImpl"

    struct_node = storage.add_compound("Node", "structExample_1_1Impl_1_1DataImpl_1_1Node", "struct")
    relations.add(struct_data_impl, struct_node)
    relations.add(data_impl_hpp, struct_node)

    assert relations.get_fully_qualified_path(struct_node) == "Example::Impl::DataImpl::Node"

    # till

    assert relations.get_fully_qualified_path(struct_node, till=ns_example) == "Impl::DataImpl::Node"
    assert relations.get_fully_qualified_path(struct_node, till=ns_impl) == "DataImpl::Node"
    assert relations.get_fully_qualified_path(struct_node, till=struct_data_impl) == "Node"

    # till - special case

    assert relations.get_fully_qualified_path(struct_node, till=struct_node) == "Example::Impl::DataImpl::Node"

    # till - special case - not a part of fq path

    assert relations.get_fully_qualified_path(struct_node, till=dir_impl) == "Example::Impl::DataImpl::Node"
