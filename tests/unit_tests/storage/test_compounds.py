from inhale.storage import compounds


def test_get_all() -> None:
    assert compounds.get_all() == (
        compounds.Class,
        compounds.Concept,
        compounds.Define,
        compounds.Directory,
        compounds.Enum,
        compounds.File,
        compounds.Function,
        compounds.Group,
        compounds.Namespace,
        compounds.Page,
        compounds.Struct,
        compounds.Typedef,
        compounds.Union,
        compounds.Variable,
    )


def test_get_data_structures() -> None:
    assert compounds.get_data_structures() == (compounds.Class, compounds.Struct)


def test_get_children() -> None:
    assert compounds.get_children() == (
        compounds.Class,
        compounds.Concept,
        compounds.Define,
        compounds.Enum,
        compounds.Function,
        compounds.Namespace,
        compounds.Struct,
        compounds.Typedef,
        compounds.Union,
        compounds.Variable,
    )


def test_get_parents() -> None:
    assert compounds.get_parents() == (compounds.Namespace, compounds.Class, compounds.Struct)
