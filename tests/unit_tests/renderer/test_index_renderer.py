import io
from unittest.mock import Mock

from inhale.storage import Storage
from inhale.renderer import get_renderer

_EXPECTED_OUTPUT = """:orphan:

.. inhale__test_project__index:

API
###

.. toctree::
   :maxdepth: 1

   _compounds
   _hierarchy
   _filesystem

"""


def test_renderer() -> None:
    with io.StringIO() as stream:
        config = Mock()
        config.project = "test_project"

        get_renderer("+index")(Storage(), config).render(stream=stream)
        assert stream.getvalue() == _EXPECTED_OUTPUT
