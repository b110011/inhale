:orphan:

.. inhale__test_project__dir_0001:

Directory ``example``
#####################

*Directory path*: ``example``

Subdirectories
**************

- :ref:`a <inhale__test_project__dir_0002>`
- :ref:`b <inhale__test_project__dir_0003>`
- :ref:`tools <inhale__test_project__dir_0005>`
- :ref:`utils <inhale__test_project__dir_0004>`
