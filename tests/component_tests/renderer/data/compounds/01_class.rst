:orphan:

.. inhale__test_project__compounds:

Types
#####

Class
*****

.. toctree::
   :maxdepth: 1

   class_Apple.rst

- :ref:`Apple <inhale__test_project__class_Apple>`
