:orphan:

.. inhale__test_project__compounds:

Types
#####

Classes
*******

.. toctree::
   :maxdepth: 1

   class_Apple.rst

- :ref:`Apple <inhale__test_project__class_Apple>`

.. toctree::
   :maxdepth: 1

   class_Banana.rst

- :ref:`Banana <inhale__test_project__class_Banana>`
