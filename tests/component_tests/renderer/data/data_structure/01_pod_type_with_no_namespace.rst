:orphan:

.. inhale__test_project__class_Apple:

.. raw:: html

   <style>.p + .w { display: block; }</style>

Class Apple
###########

- Defined in :ref:`apple.hpp <apple_8hpp>`

Class Documentation
*******************

.. doxygenclass:: Apple
   :project: test_project
