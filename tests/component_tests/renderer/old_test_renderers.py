import io
import json
from pathlib import Path
from typing import Any, Dict, List, Tuple, Type
from unittest.mock import Mock

import pytest

from inhale.config import HashType, OutputFilenameFormat
from inhale.storage import Storage
from inhale.renderer import BaseRenderer, get_renderer

"""
{
    "config": {  // optional
        "inhale_directives_args": {},
        "inhale_show_inheritance": true,
        "inhale_show_base_types": true,
        "inhale_show_derived_types": true,
        "project": "test_project"
    },
    "compound": "myawesomebaseclass",
    "storage": {
        "compounds": [
            {
                "name": "MyAwesomeBaseClass",
                "refid": "myawesomebaseclass",
                "type": "class"
            },
            {
                "name": "MyAwesomeClass",
                "refid": "myawesomeclass",
                "type": "class"
            },
            {
                "name": "my_awesome_base_class.hpp",
                "refid": "my_awesome_base_class_hpp",
                "type": "file"
            },
            {
                "name": "my_awesome_class.hpp",
                "refid": "my_awesome_class_hpp",
                "type": "file"
            }
        ],
        "includes": {  // optional
            "includes": {  // required
                "my_awesome_class_hpp": ["my_awesome_base_class_hpp"]
            },
            "included_by": {  // optional
                "my_awesome_base_class_hpp": ["my_awesome_class_hpp"]
            }
        },
        "inheritance": {  // optional
            "bases": {
                "myawesomeclass": ["myawesomebaseclass"]
            },
            "derivatives": {
                "myawesomebaseclass": ["myawesomeclass"]
            }
        },
        "relations": [
            ["my_awesome_base_class_hpp", "myawesomebaseclass"],
            ["my_awesome_class_hpp", "myawesomeclass"]
        ]
    }
}
"""


def _to_config(data: Dict[str, Any]) -> Mock:
    config = Mock()
    config.inhale_directives_args = {}
    config.inhale_show_inheritance = True
    config.inhale_show_base_types = True
    config.inhale_show_derived_types = True
    config.project = "test_project"

    config.inhale_hash_type = HashType.TIMESTAMPS
    config.inhale_output_filename_format = OutputFilenameFormat.REFID

    for k, v in data.get("config", {}).items():
        setattr(config, k, v)

    return config


def _to_storage(data: Dict[str, Any]) -> Storage:
    storage = Storage()
    storage_data: Dict[str, Any] = data["storage"]

    for compound_info in storage_data["compounds"]:
        compound = storage.add_compound(
            compound_info.pop("name"), compound_info.pop("refid"), compound_info.pop("type")
        )

        for key, value in compound_info.items():
            setattr(compound, key, value)

    includes_info = storage_data.get("includes")
    if includes_info:
        for file_refid, includes in includes_info["includes"].items():
            file_compound = storage.get_compound(file_refid)
            for include in includes:
                if not include.startswith("<"):
                    include = storage.get_compound(include)

                storage.includes.add_include(file_compound, include)

        for (file_refid, includes) in includes_info.get("included_by", {}):
            file_compound = storage.get_compound(file_refid)
            for include in includes:
                storage.includes.add_includer(file_compound, storage.get_compound(include))

    inheritance_info = storage_data.get("inheritance")
    if inheritance_info:
        for (ds_refid, ds_bases) in inheritance_info["bases"]:  # ds -> data structure
            ds_compound = storage.get_compound(ds_refid)
            for ds_base in ds_bases:
                if not ds_base.startswith("<"):
                    ds_base = storage.get_compound(ds_base)

                storage.includes.add_include(storage.get_compound(ds_compound), ds_base)

        for (ds_refid, ds_derivatives) in inheritance_info["derivatives"]:
            ds_compound = storage.get_compound(ds_refid)
            for ds_derivative in ds_derivatives:
                storage.includes.add_includer(ds_compound, storage.get_compound(ds_derivative))

    for (parent, child) in storage_data["relations"]:
        storage.relations.add(storage.get_compound(parent), storage.get_compound(child))

    return storage


def _get_test_files(name: str) -> List[Tuple[Path, Path]]:
    paths: List[Tuple[Path, Path]] = []

    data_dir = Path(__file__).parent / "data" / name
    assert data_dir.is_dir()

    for json_data_path in data_dir.glob("*.json"):
        paths.append((json_data_path, json_data_path.with_suffix(".rst")))

    paths.sort(key = lambda t: t[0])

    assert paths
    return paths


# def _format_diff_date(d: datetime) -> str:
#     return d.strftime("%Y-%m-%d %H:%M:%S")


def _run_renderer(json_data_path: Path, expected_rst_path: Path, renderer_cls: Type[BaseRenderer]) -> None:
    with json_data_path.open() as f:
        json_data = json.load(f)

    config = _to_config(json_data)
    storage = _to_storage(json_data)
    compound = storage.get_compound(json_data["compound"])

    # renderer = renderer_cls(stream, config, storage)
    # renderer.render(compound)

    # Move stream position to the start of the content so readlines works correctly.
    # test_stream.seek(0, io.SEEK_SET)

    # diff = unified_diff(
    #     expected_rst_path.open().readlines(),
    #     test_stream.readlines(),
    #     str(expected_rst_path.relative_to(Path(__file__).parent)),
    #     renderer.get_filename(test_compound) + " (memory)",
    #     _format_diff_date(datetime.fromtimestamp(expected_rst_path.stat().st_mtime)),
    #     _format_diff_date(datetime.now()),
    #     lineterm="",
    # )

    # no_diff = True
    # for line in islice(diff, 2):  # print headers
    #     print(line)
    #     no_diff = False

    # for line in diff:  # print content
    #     if not line.startswith("@@"):
    #         print(line, end="")
    #     else:
    #         print(line)

    # return no_diff

    with io.StringIO() as stream:
        renderer_cls(storage, config).render(compound, stream=stream)
        assert expected_rst_path.open().read() == stream.getvalue()


@pytest.mark.parametrize(["json_data_path", "expected_rst_path"], _get_test_files("data_structure"))
def test_data_structure_renderer(json_data_path: Path, expected_rst_path: Path) -> None:
    _run_renderer(json_data_path, expected_rst_path, get_renderer("class"))


@pytest.mark.parametrize(["json_data_path", "expected_rst_path"], _get_test_files("directory"))
def test_directory_renderer(json_data_path: Path, expected_rst_path: Path) -> None:
    _run_renderer(json_data_path, expected_rst_path, get_renderer("directory"))


# @pytest.mark.parametrize(["json_data_path", "expected_rst_path"], _get_test_files("file"))
# def test_file_renderer(json_data_path: Path, expected_rst_path: Path) -> None:
#     _run_renderer(json_data_path, expected_rst_path, get_renderer("file"))


# @pytest.mark.parametrize(["json_data_path", "expected_rst_path"], _get_test_files("namespace"))
# def test_file_renderer(json_data_path: Path, expected_rst_path: Path) -> None:
#     _run_renderer(json_data_path, expected_rst_path, get_renderer("namespace"))
