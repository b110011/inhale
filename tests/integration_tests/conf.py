# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

import datetime
import os
import sys

# -- Path setup --------------------------------------------------------------

sys.path.append(os.path.abspath("../"))

# -- Project information -----------------------------------------------------

author = "Zinchenko Serhii"
project = "Inhale Tests"

copyright = "2024, Zinchenko Serhii"

# -- General configuration ---------------------------------------------------

exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]

extensions = ["inhale"]

# -- Options for HTML output -------------------------------------------------

html_copy_source = False

# -- Extensions --------------------------------------------------------------

inhale 