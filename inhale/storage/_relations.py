__all__ = ["Relations"]

from collections import defaultdict
from typing import Mapping, Set, Sequence, Type, TypeVar, TYPE_CHECKING, Optional, List

from inhale.storage import compounds

from ._utils import create_sorted_mapping

if TYPE_CHECKING:
    from inhale.storage.compounds import BaseCompound

    Storage = Mapping[BaseCompound, Mapping[Type[BaseCompound], Set[BaseCompound]]]

    _T = TypeVar("_T", bound=BaseCompound, covariant=True)


class Relations:
    def __init__(self) -> None:
        self._parents: "Storage" = defaultdict(create_sorted_mapping)
        self._children: "Storage" = defaultdict(create_sorted_mapping)

    def add(self, parent: "BaseCompound", child: "BaseCompound") -> None:
        self._parents[child][parent.__class__].add(parent)
        self._children[parent][child.__class__].add(child)

    def get_fully_qualified_path(self, compound: "BaseCompound", *, till: Optional["BaseCompound"] = None) -> str:
        def _climb_to_root() -> List[str]:
            result: List[str] = [compound.name]
            cur_parent = compound
            while True:
                for parent_cls in compounds.get_parents():
                    parents = self.get_parents(cur_parent, parent_cls)
                    if parents:
                        cur_parent: "BaseCompound" = next(iter(parents))
                        if cur_parent == till:
                            return result

                        result.append(cur_parent.name)
                        break
                else:
                    break

            return result

        return "::".join(reversed(_climb_to_root()))

    def get_parents(self, compound: "BaseCompound", parent_compound_cls: "_T") -> Sequence["_T"]:
        return self._parents.get(compound, {}).get(parent_compound_cls, tuple())

    def get_children(self, compound: "BaseCompound", child_compound_cls: "_T") -> Sequence["_T"]:
        return self._children.get(compound, {}).get(child_compound_cls, tuple())

    def has_parents(self, compound: "BaseCompound") -> bool:
        return compound in self._parents

    def has_children(self, compound: "BaseCompound") -> bool:
        return compound in self._children
