__all__ = ["InheritanceType", "Inheritance"]

from collections import defaultdict
from enum import Enum
from typing import Dict, Set, Tuple, Union, TYPE_CHECKING, Sequence

if TYPE_CHECKING:
    from inhale.storage import compounds

    class InheritanceType(Enum):
        ...

    CS = Union[compounds.Class, compounds.Struct]
    CSS = Union[compounds.Class, compounds.Struct, str]

    Bases = Set[Tuple[CSS, InheritanceType]]
    Derivatives = Set[Tuple[CS, InheritanceType]]


class InheritanceType(Enum):
    PUBLIC = "public",
    VIRTUAL_PUBLIC = "virtual public",
    PROTECTED = "protected"
    VIRTUAL_PROTECTED = "virtual protected"
    PRIVATE = "private"
    VIRTUAL_PRIVATE = "virtual private"


class Inheritance:
    def __init__(self) -> None:
        self._bases: Dict["CS", "Bases"] = defaultdict(set)
        self._derivatives: Dict["CS", "Derivatives"] = defaultdict(set)

    def add_base(self, compound: "CS", base: "CSS", itype: InheritanceType) -> None:
        self._bases[compound].add((base, itype))

    def add_derived(self, compound: "CS", derivative: "CS", itype: InheritanceType) -> None:
        self._derivatives[compound].add((derivative, itype))

    def get_bases(self, compound: "CS") -> "Bases":
        return self._bases.get(compound, set())

    def get_derivatives(self, compound: "CS") -> "Derivatives":
        return self._derivatives.get(compound, set())
