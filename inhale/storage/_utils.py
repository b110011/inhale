__all__ = ["create_sorted_mapping"]

from collections import defaultdict
from typing import TYPE_CHECKING, Any, Mapping, Set

from sortedcontainers import SortedList

if TYPE_CHECKING:
    from inhale.storage.compounds import BaseCompound


def create_sorted_mapping() -> Mapping[Any, Set["BaseCompound"]]:
    return defaultdict(lambda: SortedList(key=lambda c: c.name))
