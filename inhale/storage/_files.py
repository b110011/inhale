__all__ = ["Files"]

from collections import defaultdict
from typing import Dict, Set, Union, Tuple, Sequence, TYPE_CHECKING

from sortedcontainers import SortedSet

if TYPE_CHECKING:
    from inhale.storage.compounds import File

    FileOrStr = Union[File, str]
    FileStorage = Tuple[Set[File], Set[str]]


def _sorted_files() -> SortedSet["File"]:
    return SortedSet(key=lambda f: f.path)


class Files:
    def __init__(self) -> None:
        self._includes: Dict["File", "FileStorage"] = defaultdict(lambda: (_sorted_files(), SortedSet()))
        self._includers: Dict["File", Set["File"]] = defaultdict(_sorted_files)

    def add_include(self, file: "File", include: "FileOrStr") -> None:
        self._includes[file][isinstance(include, str)].add(include)

    def add_includer(self, file: "File", include: "File") -> None:
        self._includers[file].add(include)

    def get_includes(self, file: "File") -> "FileStorage":
        return self._includes.get(file, ((), ()))

    def get_includers(self, file: "File") -> Sequence["File"]:
        return self._includers.get(file, set())
