__all__ = ["InheritanceType", "Storage"]

from collections import defaultdict
from typing import Dict, Generator, Sequence, Type, TypeVar, TYPE_CHECKING, Mapping

from inhale.logging import logger
from inhale.storage import compounds

from ._files import Files
from ._inheritance import Inheritance, InheritanceType
from ._relations import Relations
from ._utils import create_sorted_mapping

if TYPE_CHECKING:
    from inhale.storage.compounds import BaseCompound

    _T = TypeVar("_T", bound=BaseCompound, covariant=True)

_DOXYGEN_COMPOUND_TYPES = {
    "class": compounds.Class,
    "concept": compounds.Concept,
    "define": compounds.Define,
    "dir": compounds.Directory,
    "enum": compounds.Enum,
    "file": compounds.File,
    "function": compounds.Function,
    "group": compounds.Group,
    "namespace": compounds.Namespace,
    "page": compounds.Page,
    "struct": compounds.Struct,
    "typedef": compounds.Typedef,
    "union": compounds.Union,
    "variable": compounds.Variable,
}


class Storage:
    def __init__(self) -> None:
        self._compounds: Dict[str, "BaseCompound"] = {}  # {compound_refid: compound}
        self._compounds_by_type: Mapping[Type["BaseCompound"], Sequence["BaseCompound"]] = \
            create_sorted_mapping()  # {compound_cls: [compound]}
        self._name_collisions: Dict[str, int] = defaultdict(int)  # {compound_name: count}

        self._files = Files()
        self._inheritance = Inheritance()
        self._relations = Relations()

    @property
    def files(self) -> Files:
        return self._files

    @property
    def inheritance(self) -> Inheritance:
        return self._inheritance

    @property
    def relations(self) -> Relations:
        return self._relations

    def add_compound(self, name: str, refid: str, ctype: str) -> "BaseCompound":
        compound: "BaseCompound" = self._compounds.get(refid)
        if compound:
            return compound

        try:
            compound = _DOXYGEN_COMPOUND_TYPES[ctype](name=name, refid=refid)
        except KeyError as e:
            logger.error("%r compound is not supported at this moment!", ctype)
            raise RuntimeError("Unsupported compound!") from e

        self._compounds[compound.refid] = compound
        self._compounds_by_type[compound.__class__].add(compound)
        self._name_collisions[compound.name] += 1

        return compound

    def get_compound(self, refid: str) -> "BaseCompound":
        # NOTE(b110011): It must always be an error to get a non-existing compound.
        #   In most cases this should be an indication of some serious problem.
        return self._compounds[refid]

    def get_compounds(self, compound_cls: "_T") -> Sequence["_T"]:
        # NOTE(b110011): It's ok to not have specific compounds but must keep 'dict'
        #   untouched, however, 'defaultdict' can't provide us with this guarantee.
        return self._compounds_by_type.get(compound_cls, ())

    def get_orphan_compounds(self, compound_cls: "_T") -> Generator["_T", None, None]:
        for compound in self.get_compounds(compound_cls):
            if not self.relations.has_parents(compound):
                yield compound

    def has_name_collision(self, compound: "BaseCompound") -> bool:
        same_names_count = self._name_collisions.get(compound.name, 0)

        # NOTE(b110011): It must always be an error to get a non-existing compound.
        #   In most cases this should be an indication of some serious problem.
        if not same_names_count:
            raise KeyError(compound)

        return same_names_count != 1
