__all__ = [
    "BaseCompound",
    "BaseDataStructureCompound",
    "Class",
    "Concept",
    "Define",
    "Directory",
    "Enum",
    "File",
    "Function",
    "Group",
    "Namespace",
    "Page",
    "Struct",
    "Typedef",
    "Union",
    "Variable",
    "get_all",
    "get_all_types",
    "get_children",
    "get_data_structures",
    "get_parents",
]

from functools import cache
from pathlib import Path
from typing import Any, Dict, List, Optional, Type, Sequence, Generator
from typing import TYPE_CHECKING, Union as _Union

from attr import asdict, dataclass, field, fields


# https://stackoverflow.com/questions/76249636/class-properties-in-python-3-11
class classproperty:
    def __init__(self, func):
        self.fget = func

    def __get__(self, instance, owner):
        return self.fget(owner)


# def _internal_flag(value) -> Any:
#     return field(default=value, eq=False, hash=False, init=False, repr=False)


# def _get_internal_flag(cls: "BaseCompound", flag: str) -> bool:
#     return getattr(fields(cls), flag).default


@dataclass(eq=False, frozen=False, hash=True, kw_only=True, slots=True)
class BaseCompound:
    if TYPE_CHECKING:
        __is_child__: bool
        __is_parent__: bool
        __is_type__: bool

    name: str = field(eq=True, hash=False)
    refid: str = field(eq=False, hash=True)

    def __eq__(self, other: "BaseCompound") -> bool:
        return self.name == other.name

    def __str__(self) -> str:
        return f"{self.name!r} ({self.refid!r})"

    @classproperty
    def ctype(cls) -> str:
        return cls.type.lower()

    @classproperty
    def type(cls) -> str:
        return cls.__name__

    @property
    def special_refid(self) -> str | None:
        return None

    # @classmethod
    # def is_child(cls) -> bool:
    #     return cls.__is_child__

    # @classmethod
    # def is_parent(cls) -> bool:
    #     return cls.__is_parent__

    def to_dict(self) -> Dict[str, Any]:
        result = asdict(self)
        result["type"] = self.ctype
        return result


BaseCompound.__is_child__ = True
BaseCompound.__is_parent__ = False
BaseCompound.__is_type__ = True


@dataclass(slots=True)
class BaseDataStructureCompound(BaseCompound):
    # __is_parent__: bool = _internal_flag(True)

    @dataclass(eq=False, frozen=True, hash=False, kw_only=True, slots=True)
    class TemplateParam:
        type: _Union[Type[BaseCompound], str]
        declname: str
        defname: Optional[str]

    tparams: List["TemplateParam"] = field(eq=False, factory=list, hash=False, init=False)


BaseDataStructureCompound.__is_parent__ = True


@dataclass(frozen=True)
class Class(BaseDataStructureCompound):
    pass


@dataclass(frozen=True)
class Concept(BaseCompound):
    pass


@dataclass(frozen=True)
class Define(BaseCompound):
    pass


@dataclass(hash=True)
class Directory(BaseCompound):
    # __is_type__: bool = _internal_flag(False)
    # __is_parent__: bool = _internal_flag(True)

    path: Path = field(eq=False, hash=False, init=False)


Directory.__is_type__ = False
Directory.__is_parent__ = True


@dataclass(frozen=True)
class Enum(BaseCompound):
    pass


@dataclass(hash=True)
class File(BaseCompound):
    # __is_type__: bool = _internal_flag(False)
    # __is_parent__: bool = _internal_flag(True)

    lang: str = field(eq=False, hash=False, init=False)
    path: Path = field(eq=False, hash=False, init=False)

    # get_brief_description: bool = field(default=lambda: None, eq=False, hash=False, init=False)
    # get_detailed_description: bool = field(default=lambda: None, eq=False, hash=False, init=False)

    @property
    def special_refid(self) -> str:
        return self.refid + "__listing"


File.__is_type__ = False
File.__is_parent__ = True


@dataclass(hash=True)
class Function(BaseCompound):
    return_type: str
    params: List[str] = field(eq=False, factory=list, hash=False, init=False)
    tparams: List[str] = field(eq=False, factory=list, hash=False, init=False)


@dataclass(frozen=True)
class Group(BaseCompound):
    # __is_type__: bool = _internal_flag(False)
    # __is_parent__: bool = _internal_flag(True)
    pass


Group.__is_type__ = False
Group.__is_parent__ = True


@dataclass(frozen=True)
class Namespace(BaseCompound):
    # __is_parent__: bool = _internal_flag(True)
    pass


Namespace.__is_parent__ = True


@dataclass(hash=True)
class Page(BaseCompound):
    # __is_type__: bool = _internal_flag(False)
    # __is_parent__: bool = _internal_flag(True)

    path: Path = field(eq=False, hash=False, init=False)
    title: str = field(eq=False, hash=False, init=False)


Page.__is_type__ = False
Page.__is_parent__ = True


@dataclass(frozen=True)
class Struct(BaseDataStructureCompound):
    pass


@dataclass(frozen=True)
class Typedef(BaseCompound):
    pass


@dataclass(frozen=True)
class Union(BaseCompound):
    pass


@dataclass(frozen=True)
class Variable(BaseCompound):
    pass


_CompoundsGenerator = Generator[Type[BaseCompound], None, None]


def _get_all() -> _CompoundsGenerator:
    for cls in BaseCompound.__subclasses__():
        if cls is not BaseDataStructureCompound:
            yield cls

    for cls in BaseDataStructureCompound.__subclasses__():
        yield cls


def _get_all_types(flag: Optional[str] = None) -> _CompoundsGenerator:
    for cls in _get_all():
        # if _get_internal_flag(cls, "__is_type__") and _get_internal_flag(cls, flag):
        if cls.__is_type__ and (not flag or getattr(cls, flag)):
            yield cls


def _get_sorted_compounds(g: _Union[_CompoundsGenerator, List]) -> Sequence[Type[BaseCompound]]:
    return tuple(sorted(list(g), key=lambda cls: cls.__name__))


@cache
def get_all() -> Sequence[Type[BaseCompound]]:
    return _get_sorted_compounds(_get_all())


@cache
def get_all_types() -> Sequence[Type[BaseCompound]]:
    return _get_sorted_compounds(_get_all_types())


@cache
def get_data_structures() -> Sequence[Type[BaseCompound]]:
    return _get_sorted_compounds(BaseDataStructureCompound.__subclasses__())


@cache
def get_children() -> Sequence[Type[BaseCompound]]:
    return _get_sorted_compounds(_get_all_types("__is_child__"))


@cache
def get_parents() -> Sequence[Type[BaseCompound]]:
    return (Namespace, Class, Struct)
