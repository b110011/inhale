from typing import TYPE_CHECKING, Dict, Tuple

from sphinx.config import ENUM
from sphinx.errors import ExtensionError
from sphinx.util import logging

# from inhale.config import ConfigAdaptor, DoxygenConfigOptions, ProjectDeps, ProjectInfo
# from inhale.hash import HashType, calculate_dir_hash
# from inhale import doxygen

if TYPE_CHECKING:
    from sphinx.application import Sphinx
    from sphinx.config import Config
    from sphinx.environment import BuildEnvironment

logger = logging.getLogger(__package__)


# class Inhale:
#     config_adaptor: ConfigAdaptor
#     ctx: _backend.Context


# def _setup_dependency(app: "Sphinx") -> bool:
#     used_dep = ""
#     counter = 0

#     for dep in ("breathe", "docleaf"):
#         if dep in app.config.extensions:
#             app.setup_extension(dep)

#             used_dep = dep
#             counter += 1

#     if counter == 0:
#         raise ExtensionError("'inhale' requires either 'breathe' or 'docleaf' extension")
#     elif counter == 2:
#         raise ExtensionError("'breathe' extension is mutually exclusive with 'docleaf' one")

#     return used_dep


# def _handle_inited_config(app: "Sphinx", config: "Config") -> None:
#     logger.info("Normalizing dependencies for doxygen projects")

#     a: ConfigAdaptor = app.inhale.config_adaptor
#     a.raw_projects = doxygen.normalize_dependencies(a.raw_projects, a.dependencies)


# def _handle_inited_builder(app: "Sphinx") -> None:
#     inhale: Inhale = app.inhale

#     if not hasattr(app.env, "inhale_projects_hashes"):
#         app.env.inhale_projects_hashes = {}
#     projects_hashes: dict[str, dict[str, int]] = app.env.inhale_projects_hashes

#     doxygen_options = inhale.config_adaptor.doxygen_config_options
#     doxygen.validate_options(doxygen_options)

#     for project, info in inhale.config_adaptor.projects.items():
#         skip_render = False
#         src_dir = info["root"]
#         xml_dir = info["xml"]
#         rst_dir = info["rst"]

#         logger.info("Calculating hashes for {} project directories...", project)

#         src_dir_hash = calculate_dir_hash(src_dir)
#         rst_dir_hash = calculate_dir_hash(rst_dir)
#         project_hashes = projects_hashes.get(project, {})

#         if src_dir_hash != project_hashes.get("root"):
#             logger.warning("Hashes for {} directory ({} project) has changed!", src_dir_hash, project)
#             logger.info("Running doxygen for {} project files...", project)

#             doxygen.run(src_dir, xml_dir, doxygen_options)
#         elif rst_dir_hash == project_hashes.get("rst"):
#             skip_render = True

#         inhale.ctx.set_current_project(project)

#         logger.info("Parsing doxygen XML files for {} project...", project)
#         _backend.parse_doxygen_output(
#             inhale.ctx,
#             _backend.ParserConfig(src_dir=src_dir, xml_dir=xml_dir)
#         )

#         if skip_render:
#             continue

#         logger.warning("Hashes for {} directory ({} project) has changed!", src_dir_hash, project)
#         logger.info("Rendering RST files for {} project...", project)

#         _backend.render_rst_files(
#             inhale.ctx,
#             _backend.RenderConfig(xml_dir=xml_dir, rst_dir=rst_dir)
#         )

#         projects_hashes[project] = {"root": src_dir_hash, "rst": rst_dir_hash}


# def _handle_consistency_check(app: "Sphinx", env: "BuildEnvironment") -> None:
#     info: ProjectInfo = app.inhale.config_adaptor.projects
#     hashes: dict = env.inhale_projects_hashes

#     for project in list(hashes.keys()):
#         if project not in info:
#             del hashes[project]


# def _handle_finished_build(app: "Sphinx", exception: Exception) -> None:
#     # NOTE(b110011): Explicitly release backend logger to avoid memory leaks.
#     _backend.reset_logger()


# def setup(app: "Sphinx") -> dict:
#     dep = _setup_dependency(app)

#     inhale = Inhale()
#     inhale.config_adaptor = ConfigAdaptor(dep, app)
#     inhale.ctx = _backend.Context()

#     app["inhale"] = inhale

#     app.add_config_value("inhale_doxygen_config_options", None, "env", DoxygenConfigOptions)
#     app.add_config_value("inhale_hash_type", "auto", "env", ENUM([e.value for e in HashType]))
#     app.add_config_value("inhale_project_deps", None, "env", ProjectDeps)

#     app.add_config_value(
#         "inhale_directives_args",
#         {
#             "doxygenclass": ("members", "protected-members", "undoc-members"),
#         },
#         "env",
#         Dict[str, Tuple[str]]
#     )
#     app.add_config_value("inhale_show_inheritance", True, "env", bool)
#     app.add_config_value("inhale_show_base_types", False, "env", bool)
#     app.add_config_value("inhale_show_derived_types", True, "env", bool)

#     app.connect("config-inited", _handle_inited_config)
#     app.connect("builder-inited", _handle_inited_builder)
#     app.connect("env-check-consistency", _handle_consistency_check)
#     app.connect("build-finished", _handle_finished_build)

#     _backend.set_logger(logging.getLogger(f"{__package__}.backend"))

#     return {
#         "version": "0.0.1",
#         "parallel_read_safe": True,
#         "parallel_write_safe": True
#     }
