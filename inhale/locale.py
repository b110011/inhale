__all__ = ["_", "ngettext", "setup"]

from pathlib import Path

from sphinx.application import Sphinx
from sphinx.locale import is_translator_registered, get_translator, get_translation
from sphinx.errors import ExtensionError

_CATALOG = __package__.split(".")[0]
_NAMESPACE = "general"

_ = get_translation(_CATALOG, _NAMESPACE)


def ngettext(msg1: str, msg2: str, n: int) -> str:
    if not is_translator_registered(_CATALOG, _NAMESPACE):
        return msg1 if n == 1 else msg2
    else:
        translator = get_translator(_CATALOG, _NAMESPACE)
        return translator.ngettext(msg1, msg2, n)


def setup(app: Sphinx) -> None:
    locale_dir = Path(__file__).absolute() / "locales"
    app.add_message_catalog(_CATALOG, locale_dir)
