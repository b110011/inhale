__all__ = ["logger"]

from sphinx.util import logging

logger = logging.getLogger("inhale")
