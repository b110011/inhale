from pathlib import Path
from typing import TYPE_CHECKING

from . import _section_parser

if TYPE_CHECKING:
    from xml.etree.ElementTree import Element as XmlElement

    from inhale.storage import compounds, Storage


def parse(storage: "Storage", file: "compounds.File", eroot: "XmlElement") -> None:
    cdef = eroot.find("./doxygen/compounddef")

    file.lang = cdef["language"]
    file.path = Path(cdef.find("./location")["file"])

    for include in cdef.iter("includes"):
        include_refid = include.get("refid")
        if include_refid:
            include_compound = storage.get_compound(include_refid)
            storage.includes.add_include(file, include_compound)

            # TODO(b110011): We might find include by it's path.
        else:
            storage.includes.add_include(file, include.text)

    for include in cdef.iter("includedby"):
        include_compound = storage.get_compound(include["refid"])
        storage.includes.add_includer(file, include_compound)

    for ic in cdef.iter("innerclass"):
        ic_compound = storage.get_compound(ic["refid"])
        storage.relations.add(file, ic_compound)

    # NOTE(b110011): Skip 'innernamespace' section.

    _section_parser.parse(storage, file, cdef)
