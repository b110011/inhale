from pathlib import Path
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from xml.etree.ElementTree import Element as XmlElement

    from inhale.storage import compounds, Storage


def parse(storage: "Storage", page: "compounds.Page", eroot: "XmlElement") -> None:
    cdef = eroot.find("./doxygen/compounddef")

    page.path = Path(cdef.find("./location")["file"])
    page.title = cdef.find("title").text

    for ip in cdef.iter("innerpage"):
        ip_compound = storage.get_compound(ip["refid"])
        storage.relations.add(dir, ip_compound)
