from xml.etree.ElementTree import Element as XmlElement
from typing import TYPE_CHECKING

from inhale.storage import InheritanceType
from inhale.storage.compounds import BaseDataStructureCompound

if TYPE_CHECKING:
    from inhale.storage import Storage


_INHERITANCE_TYPE = {
    ("private", "non-virtual"): InheritanceType.PRIVATE,
    ("private", "virtual"): InheritanceType.VIRTUAL_PRIVATE,
    ("protected", "non-virtual"): InheritanceType.PROTECTED,
    ("protected", "virtual"): InheritanceType.VIRTUAL_PROTECTED,
    ("public", "non-virtual"): InheritanceType.PUBLIC,
    ("public", "virtual"): InheritanceType.VIRTUAL_PUBLIC,
}


def _get_itype(e: XmlElement) -> InheritanceType:
    return _INHERITANCE_TYPE[(e["prot"], e["virt"])]


def parse(storage: "Storage", ds: BaseDataStructureCompound, eroot: XmlElement) -> None:
    cdef = eroot.find("./doxygen/compounddef")

    for tparam in cdef.findall("./templateparamlist/param"):
        # NOTE(svenevs): Doxygen seems to produce unreliable results.
        #
        #   For example, sometimes you will get 'param.type <- class X' with empty
        #   'declname' and 'defname', and sometimes you will get 'param.type <- class'
        #   and 'decl' name 'X'. Similar behavior is observed with 'typename X'.
        #
        #   These are generally just ignored (falling in the broader category of a
        #   typename).
        #
        #   Sometimes you will get a refid in the type, so deal with that as they
        #   come too (yay)!
        tparam_declname = tparam.find("declname").text
        tparam_defname = tparam.find("defname").text

        # TODO(svenevs): this doesn't seem to happen, should probably investigate
        #   more do something with 'param.defval' ?

        # NOTE(svenevs): When 'decl_n' and 'def_n' are the same, this means no
        #   explicit default template parameter is given. This will ultimately
        #   mean that 'def_n' is set to None for consistency.
        if tparam_declname == tparam_defname:
            tparam_defname = None

        tparam_type_refid = tparam.find("type").get("refid")
        if tparam_type_refid:
            ds.tparams.append(
                BaseDataStructureCompound.TemplateParam(
                    type=storage.get_compound(tparam_type_refid),
                    declname=tparam_declname,
                    defname=tparam_defname
                )
            )
        else:
            ds.tparams.append(
                BaseDataStructureCompound.TemplateParam(
                    type=tparam.find("type").text,
                    declname=tparam_declname,
                    defname=tparam_defname
                )
            )

    for bcf in cdef.iter("basecompoundref"):
        bcf_refid = bcf.get("refid")
        itype = _get_itype(bcf)
        if bcf_refid:
            bcf_compound = storage.get_compound(bcf_refid)
            storage.inheritance.add_base(ds, bcf_compound, itype)
        else:
            storage.inheritance.add_base(ds, bcf.text, itype)

    for dcf in cdef.iter("derivedcompoundref"):
        dcf_compound = storage.get_compound(dcf["refid"])
        storage.inheritance.add_derived(ds, dcf_compound, _get_itype(dcf))

    for ic in cdef.iter("innerclass"):
        ic_compound = storage.get_compound(ic["refid"])
        storage.relations.add(ds, ic_compound)
