from pathlib import Path
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from xml.etree.ElementTree import Element as XmlElement

    from inhale.storage import compounds, Storage


def parse(storage: "Storage", folder: "compounds.Directory", eroot: "XmlElement") -> None:
    cdef = eroot.find("./doxygen/compounddef")

    folder.path = Path(folder.name)
    folder.name = folder.path.name

    for idir in cdef.iter("innerdir"):
        idir_compound = storage.get_compound(idir["refid"])
        storage.relations.add(folder, idir_compound)

    for ifile in cdef.iter("innerfile"):
        ifile_compound = storage.get_compound(ifile["refid"])
        storage.relations.add(folder, ifile_compound)
