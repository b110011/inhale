from typing import TYPE_CHECKING

from . import _section_parser

if TYPE_CHECKING:
    from xml.etree.ElementTree import Element as XmlElement

    from inhale.storage import compounds, Storage


def parse(storage: "Storage", ns: "compounds.Namespace", eroot: "XmlElement") -> None:
    cdef = eroot.find("./doxygen/compounddef")

    # NOTE(b110011): Seems like namespaces in 'index.xml' don't list 'innerclass'
    #   and 'innernamespace' in their 'members' but list 'function' and 'variable'.
    #   We need to bind missing relations here.

    for ic in cdef.iter("innerclass"):
        ic_compound = storage.get_compound(ic["refid"])
        storage.relations.add(ns, ic_compound)

    for ins in cdef.iter("innernamespace"):
        ifile_compound = storage.get_compound(ins["refid"])
        storage.relations.add(ns, ifile_compound)

    _section_parser.parse(storage, ns, cdef)
