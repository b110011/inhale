__all__ = ["parse"]

import xml.etree.ElementTree as XmlTree
from typing import Callable, Mapping, Type, Sequence, TYPE_CHECKING

from inhale.logging import logger
from inhale.storage import compounds

from . import (
    _data_structure_parser,
    _directory_parser,
    _file_parser,
    _index_parser,
    _namespaces_parser,
    _page_parser
)

if TYPE_CHECKING:
    from pathlib import Path

    from inhale.storage import Storage
    from inhale.storage.compounds import BaseCompound

_PARSERS: Mapping["BaseCompound", Callable[["Storage", "BaseCompound", XmlTree.Element], None]] = {
    compounds.Class: _data_structure_parser.parse,
    compounds.Concept: None,
    compounds.Define: None,
    compounds.Directory: _directory_parser.parse,
    compounds.Enum: None,
    compounds.File: _file_parser.parse,
    compounds.Function: None,
    compounds.Group: None,
    compounds.Namespace: _namespaces_parser.parse,
    compounds.Page: _page_parser.parse,
    compounds.Struct: _data_structure_parser.parse,
    compounds.Typedef: None,
    compounds.Union: None,
    compounds.Variable: None,
}


def parse(storage: "Storage", doxygen_xml_output: "Path") -> None:
    logger.debug("Parsing index file")

    et =  XmlTree.parse(doxygen_xml_output / "index.xml")
    _index_parser.parse(storage, et.getroot())

    for compound_cls, parse in _PARSERS.items():
        compounds: Sequence["BaseCompound"] = storage.get_compounds(compound_cls)

        logger.info("Parsing ~%s %ss", len(compounds), compound_cls.ctype)

        for compound in compounds:
            logger.debug("Parsing %s compound", compound)

            path = (doxygen_xml_output / compound.refid).with_suffix(".xml")
            et = XmlTree.parse(path)

            parse(storage, compound, et.getroot())
