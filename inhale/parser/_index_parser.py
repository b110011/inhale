from xml.etree.ElementTree import Element as XmlElement
from typing import Type, TYPE_CHECKING

from inhale.storage import compounds

if TYPE_CHECKING:
    from inhale.storage import Storage
    from inhale.storage.compounds import BaseCompound


def _add_compound(storage: "Storage", e: "XmlElement") -> "BaseCompound":
    return storage.add_compound(e.find("name").text, e["refid"], e["type"])

def parse(storage: "Storage", eroot: "XmlElement") -> None:
    for c in eroot.findall("./doxygenindex/compound"):
        try:
            compound = _add_compound(storage, c)

            # NOTE(svenevs): For things like files and namespaces, a "member"
            #   list will include things like defines, enums, etc.

            #   For other type we don't need to pay attention because "breathe"
            #   will handle all that.
            if isinstance(compound, (compounds.File, compounds.Namespace)):
                for m in c.iter("member"):
                    storage.relations.add(compound, _add_compound(storage, m))
        except IndexError:
            pass
