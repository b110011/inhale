from pathlib import Path
from typing import Type, cast, TYPE_CHECKING

from inhale.logging import logger
from inhale.storage import compounds

if TYPE_CHECKING:
    from xml.etree.ElementTree import Element as XmlElement

    from inhale.storage import Storage
    from inhale.storage.compounds import BaseCompound

# NOTE(b110011): There is a weird behavior which appears at least on 1.9.4 version
#   of doxygen.
#
#   Below code doesn't define 'add' method at all. You don't have it neither in
#   'sectiondef.memberdef' nor in 'programlisting'. I guess that's why doxygen
#   also doesn't add that method to file 'members' in 'index.xml'.
#
#   But it appears in 'sectiondef.memberdef' of the parent namespace and even has
#   a correct location.
#
#       #ifndef __EXAMPLE_HPP__
#       #define __EXAMPLE_HPP__
#
#       namespace Example {
#
#       template<typename T>
#       T add(T a, T b)
#       {
#           return a + b;
#       }
#
#       } // namespace Example
#
#       #endif // __EXAMPLE_HPP__


def parse(storage: "Storage", parent: "BaseCompound", cdef: "XmlElement") -> None:
    for sdef in cdef.iter("sectiondef"):
        for mdef in sdef.iter("memberdef"):
            try:
                mdef_compound = storage.get_compound(mdef["id"])
            except IndexError:
                mdef_compound = storage.add_compound(
                    mdef["id"], mdef.find("name").text, mdef["type"]
                )

                file_path = Path(cdef.find("./location")["file"])
                for file_compound in storage.get_compounds(compounds.File):
                    if file_compound.path == file_path:
                        storage.relations.add(file_compound, mdef_compound)
                        break
                else:
                    logger.warning(
                        "Failed to find compound for %r file which appeared in %e!",
                        file_path,
                        cdef["id"]
                    )

            if sdef["kind"] == "func":
                func_compound = cast(compounds.Function, mdef_compound)
                func_compound.return_type = mdef.find("type").text

                for p in mdef.iter("param"):
                    func_compound.params.append(p.find("type").text)

                tparams = mdef.find("templateparamlist")
                if tparams:
                    for p in tparams.iter("param"):
                        func_compound.tparams.append(p.find("type").text)

            storage.relations.add(parent, mdef_compound)
