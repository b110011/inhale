from enum import StrEnum
from typing import TYPE_CHECKING, Any, Dict, Protocol, TypeAlias, Tuple, Union

if TYPE_CHECKING:
    from sphinx.application import Sphinx

DocLeafProjects: TypeAlias = dict[str, dict[str, Any]]
DoxygenConfigOptions: TypeAlias = dict[str, str]
ProjectDeps: TypeAlias = dict[str, list[str]]
ProjectInfo: TypeAlias = dict[str, dict[str, Any]]


class HashType(StrEnum):
    AUTO = "auto"
    MD5 = "md5"
    MD5_NO_WHITESPACES = "md5-nws"
    TIMESTAMPS = "ts"


class OutputFilenameFormat(StrEnum):
    AUTO = "auto"
    MD5 = "md5"
    REFID = "refid"


class Config(Protocol):
    @property
    def inhale_hash_type(self) -> HashType:
        ...

    @property
    def inhale_directives_args(self) -> Dict[str, Tuple[str]]:
        ...

    @property
    def inhale_show_includers(self) -> bool:
        ...

    @property
    def inhale_show_inheritance(self) -> bool:
        ...

    @property
    def inhale_show_base_types(self) -> bool:
        ...

    @property
    def inhale_show_derived_types(self) -> bool:
        ...

    @property
    def inhale_output_filename_format(self) -> OutputFilenameFormat:
        ...

    @property
    def project(self) -> str:
        ...


class ConfigAdaptor:
    def __init__(self, app: "Sphinx", dep: str) -> None:
        self._app = app
        self._dep = dep

    def _get(self, name: str) -> Any:
        return self._app.config[name]

    @property
    def project(self) -> str:
        ...
