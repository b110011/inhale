__all__ = ["check_version", "validate_options", "run"]

import os
from typing import TYPE_CHECKING, Any

if TYPE_CHECKING:
    from .config import DoxygenConfigOptions


def check_version() -> None:
    semver = os.popen("doxygen --version").read().split(".")

    try:
        semver = (int(semver[0]), int(semver[1]), int(semver[2]))
    except (IndexError, ValueError):
        raise RuntimeError()

    if semver < (1, 9, 0):
        raise RuntimeError()


# NOTE(b110011): To make breathe_projects_depends work correctly, we need to
#   sort breathe_projects in a way, that dependencies come before their real
#   usage.
#
#   For example we have a such dependencies:
#     breathe_projects_depends = {
#      "A": ["D"],
#      "B": [],
#      "C": [],
#      "D": ["B", "C"]
#     }
#   After analyzing dependencies breathe_projects will be adjusted to below
#   version:
#     breathe_projects = {
#      "B": "...",
#      "C": "...",
#      "D": "...",
#      "A": "...",
#     }
def normalize_dependencies(
    projects_info: dict[str, Any],
    projects_deps: dict[str, list[str]]
) -> list[str]:
    result = []

    def _walk(project, dependencies):
        for dependency in dependencies:
            _walk(dependency, projects_deps[project])

        if project not in result:
            result.append(project)

    for project, dependencies in projects_deps.items():
        _walk(project, dependencies)

    return {project: projects_info[project] for project in result}


_RESERVED_DOXYGEN_CONFIG_OPTIONS = {
    "INPUT": "",
    "GENERATE_XML": True,
    "OUTPUT": ""
}
_DEFAULT_DOXYGEN_CONFIG_OPTIONS = {
    "ENABLE_PREPROCESSING": True,
    "EXCLUDE": ("*.ipp"),
    "GENERATE_HTML": False,
    "GENERATE_LATEX": False,
    "HIDE_SCOPE_NAMES": False,
    "SHOW_NAMESPACES": False,
    "SHOW_USED_FILES": False,
    "VERBATIM_HEADERS": False,
    # Change to FAIL_ON_WARNINGS when we have doxygen version >= 1.9.0.
    "WARN_AS_ERROR": True
}

# DISABLE_INDEX - must be false

# SHORT_NAMES - must be false
# https://stackoverflow.com/questions/8145278/making-stable-names-for-doxygen-html-docs-pages
# they are based on the name, and for functions also on (a hash of) the parameters.

def validate_options(opts: DoxygenConfigOptions) -> None:
    pass


def _dump_options(src: str, out: str, opts: DoxygenConfigOptions) -> str:
    pass


def run(src: str, out: str, opts: DoxygenConfigOptions) -> None:
    config = _dump_options(src, out, opts)
