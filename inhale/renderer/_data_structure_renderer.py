from typing import TYPE_CHECKING

from inhale.locale import _, ngettext
from inhale.storage import compounds

from ._base import BaseRenderer, template

if TYPE_CHECKING:
    from inhale.storage.compounds import BaseDataStructureCompound


class DataStructRenderer(BaseRenderer):
    def do_render(self, ds: "BaseDataStructureCompound") -> None:
        self.writenl(".. raw:: html")
        self.writenl()
        self.writenl("   <style>.p + .w {{ display: block; }}</style>")
        self.writenl()

        self.h1("{}{} {}", template(ds), _(ds.type), ds.name)
        self.writenl()

        for file in self.storage.relations.get_parents(ds, compounds.File):
            self.writenl("- {} :ref:`{} <{}>`", _("Defined in"), file.path, file.refid)
            self.writenl()

        self.h2("{} {}", _(ds.type), _("Documentation"))
        self.writenl()

        self.render_doxygen_directive(ds)

        base_types, derived_types = [], []
        for ctype in compounds.get_data_structures():
            base_types += self.storage.relations.get_parents(ds, ctype)
            derived_types += self.storage.relations.get_children(ds, ctype)

        if not self.config.inhale_show_inheritance or not (base_types or derived_types):
            return

        self.writenl(_("Inheritance"))
        self.writenl()

        if self.config.inhale_show_base_types and base_types:
            ngettext("Base Type", "Base Types", len(base_types))
            self.writenl()

            for (base, itype) in self.storage.inheritance.get_bases(ds):
                if isinstance(base, str):
                    tmpl = "- ``{itype} {fq_base}``"
                else:
                    tmpl = "- ``{itype}`` :ref:`{fq_base} <{ref}>`"

                fq_base = self.storage.relations.get_fully_qualified_path(base)
                self.writenl(tmpl, itype=itype, fq_base=fq_base, ref=self.get_ref(base))

            self.writenl()

        if self.config.inhale_show_derived_types and derived_types:
            ngettext("Derived Type", "Derived Types", len(derived_types))
            self.writenl()

            for (derivative, itype) in self.storage.inheritance.get_derivatives(ds):
                fq_base = self.storage.relations.get_fully_qualified_path(derivative)
                self.writenl("- ``{}`` :ref:`{} <{}>`", itype, fq_base, self.get_ref(derivative))

            self.writenl()
