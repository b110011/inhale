from typing import List, Tuple

from inhale.locale import _

from ._base import BaseRenderer


class IndexRenderer(BaseRenderer):
    index_priority = -1
    index_filename = "index"
    index_refid = "index"

    def do_render(self) -> None:
        self.h1(_("API"))
        self.writenl()
        self.writenl(".. toctree::")
        self.writenl("   :maxdepth: 1")
        self.writenl()

        renderers: List[Tuple[int, str]] = []
        for cls in BaseRenderer.__subclasses__():
            if (cls is not self.__class__) and cls.index_filename:
                renderers.append((cls.index_priority, cls.index_filename))

        for idx, filename in sorted(renderers, key=lambda t: t[0]):
            self.writenl("   {}", filename)

        self.writenl()
