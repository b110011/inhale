from typing import TypeVar, Sequence, TYPE_CHECKING

from inhale.locale import _
from inhale.storage import compounds

from ._base import BaseRenderer

if TYPE_CHECKING:
    from inhale.storage.compounds import BaseCompound

    _T = TypeVar("_T", bound=BaseCompound, covariant=True)


class DirectoryRenderer(BaseRenderer):
    def do_render(self, folder: compounds.Directory) -> None:
        self.h1("{} ``{}``", _("Directory"), folder.name)
        self.writenl()

        self.render_parent_dir(folder, _("Go to the parent directory."))

        self.writenl("*{}*: ``{}``", _("Directory path"), folder.path)
        self.writenl()

        self._render_l1_children(folder, _("Subdirectory"), _("Subdirectories"), compounds.Directory)
        self._render_l1_children(folder, _("File"), _("Files"), compounds.File)

    def _render_l1_children(
        self, folder: compounds.Directory, title1: str, title2: str, child_compound_cls: "_T"
    ) -> None:
        children: Sequence["_T"] = self.storage.relations.get_children(folder, child_compound_cls)
        if not children:
            return

        self.h2(title1 if len(children) == 1 else title2)
        self.writenl()

        for child in children:
            self.writenl("- :ref:`{} <{}>`", child.name, self.get_ref(child))

        self.writenl()
