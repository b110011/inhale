__all__ = ["BaseRenderer", "get_renderer", "render"]

from typing import Dict, Mapping, Type, Sequence, TYPE_CHECKING

from inhale.logging import logger
from inhale.storage import compounds

from ._base import BaseRenderer, check_renderers_compliance, get_index_renderers
from ._compounds_renderer import CompoundsRenderer
from ._data_structure_renderer import DataStructRenderer
from ._directory_renderer import DirectoryRenderer
from ._file_renderer import FileListingRenderer, FileRenderer, FileRendererFacade
from ._filesystem_renderer import FileSystemRenderer
from ._hierarchy_renderer import HierarchyRenderer
from ._index_renderer import IndexRenderer
from ._namespace_renderer import NamespaceRenderer
from ._simple_renderer import SimpleRenderer

if TYPE_CHECKING:
    from inhale.config import Config
    from inhale.storage import Storage
    from inhale.storage.compounds import BaseCompound

check_renderers_compliance()

_RENDERERS_BY_TYPES: Mapping[Type["BaseCompound"], Type[BaseRenderer]] = {
    compounds.Class: DataStructRenderer,
    compounds.Concept: SimpleRenderer,
    compounds.Define: SimpleRenderer,
    compounds.Directory: DirectoryRenderer,
    compounds.Enum: SimpleRenderer,
    compounds.File: FileRendererFacade,
    compounds.Function: SimpleRenderer,
    compounds.Group: None,
    compounds.Namespace: NamespaceRenderer,
    compounds.Page: None,
    compounds.Struct: DataStructRenderer,
    compounds.Typedef: SimpleRenderer,
    compounds.Union: SimpleRenderer,
    compounds.Variable: SimpleRenderer,
}

_RENDERERS: Dict[str, Type[BaseRenderer]] = {
    compound_cls.ctype: renderer_cls for compound_cls, renderer_cls in _RENDERERS_BY_TYPES.items()
}
_RENDERERS.update(
    {
        "+compounds": CompoundsRenderer,
        "+file": FileRenderer,
        "+file_listing": FileListingRenderer,
        "+filesystem": FileSystemRenderer,
        "+hierarchy": HierarchyRenderer,
        "+index": IndexRenderer,
    }
)


def get_renderer(ctype: str) -> Type[BaseRenderer]:
    return _RENDERERS[ctype]


def render(storage: "Storage", config: "Config") -> None:
    logger.debug("Rendering RST files")

    for compound_cls, renderer_cls in _RENDERERS_BY_TYPES.items():
        compounds: Sequence["BaseCompound"] = storage.get_compounds(compound_cls)

        logger.info("Rendering %s %ss", len(compounds), compound_cls.ctype)

        renderer: BaseRenderer = renderer_cls(storage, config)
        for compound in compounds:
            logger.debug("Rendering %s compound", compound)
            renderer.render(compound)

    for renderer_cls in get_index_renderers():
        logger.info("Rendering %s index", renderer_cls.index_refid)
        renderer_cls(storage, config).render()
