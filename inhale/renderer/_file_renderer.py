from pathlib import Path
from typing import Sequence, TYPE_CHECKING

from inhale.locale import _
from inhale.storage import compounds

from ._base import BaseRenderer

if TYPE_CHECKING:
    from inhale.config import Config
    from inhale.storage import Storage
    from inhale.storage.compounds import BaseCompound
    from inhale.storage import compounds


class FileRenderer(BaseRenderer):
    def do_render(self, file: compounds.File) -> None:
        self.h1("{} {}", _(file.type), file.name)
        self.writenl()

        self.render_parent_dir(file, _("Go to the parent directory of this file."))

        self.writenl(":ref:`{} <{}__listing>`.", _("Go to the source code of this file."), self.get_ref(file))
        self.writenl()

        self.render_content(file, use_fq_names=True)

        compound_includes, str_includes = self.storage.files.get_includes(file)
        if compound_includes or str_includes:
            self.h2(_("Includes"))
            self.writenl()

            for include in compound_includes:
                self.writenl("- :ref:`{} <{}>`", include.path, self.get_ref(include))

            for include in str_includes:
                self.writenl("- ``{}``", include)

            self.writenl()

        includers = self.storage.files.get_includers(file)
        if includers and self.config.inhale_show_includers:
            self.h2(_("Included by following files"))
            self.writenl()

            for includer in includers:
                self.writenl("- :ref:`{} <{}>`", includer.path, self.get_ref(includer))

            self.writenl()


class FileListingRenderer(BaseRenderer):
    def do_render(self, file: compounds.File) -> None:
        self.h2(_("Program Listing"))
        self.writenl()

        self.render_parent_link(_("Go to the documentation of this file."), super().get_ref(file))

        # TODO(b110011): The file name is usually relative to the current file’s path.
        #   However, if it is absolute (starting with /), it is relative to the
        #   top source directory.
        self.writenl(".. literalinclude:: {}", file.path)
        self.writenl()

    # NOTE(b110011): Redefine this to get correct file name.
    def get_file_name(self, compound: "BaseCompound") -> Path:
        old_file_name = super().get_file_name(compound)
        return old_file_name.with_stem(old_file_name.stem + "__listing")

    # NOTE(b110011): Redefine this so `render` method can generate correct ref.
    def get_ref(self, compound: "BaseCompound") -> str:
        return super().get_ref(compound) + "__listing"


class FileRendererFacade(BaseRenderer):
    def __init__(self, storage: "Storage", config: "Config") -> None:
        self._renderers: Sequence[BaseRenderer] = (
            cls(storage, config) for cls in (FileRenderer, FileListingRenderer)
        )

    def do_render(self, file: compounds.File) -> None:
        for render in self._renderers:
            render.do_render(file)
