from typing import TYPE_CHECKING, List, Sequence, Optional

from inhale.locale import _
from inhale.storage import compounds

from ._base import BaseRenderer, get_type_plural

if TYPE_CHECKING:
    from inhale.storage.compounds import BaseCompound


class CompoundsRenderer(BaseRenderer):
    index_priority = 0
    index_filename = "_compounds"
    index_refid = "compounds"

    def do_render(self) -> None:
        self.h1(_("Types"))
        self.writenl()

        common_ns = self._get_common_namespace()
        if common_ns:
            self.writenl(
                "**{}** {}",
                _("Common namespace for all types is:"),
                self.storage.relations.get_fully_qualified_path(common_ns[-1])
            )

        for compound_cls in compounds.get_all_types():
            typed_compounds: Sequence["BaseCompound"] = self.storage.get_compounds(compound_cls)
            if not typed_compounds:
                continue

            self.h2(get_type_plural(compound_cls, len(typed_compounds)))
            self.writenl()

            for compound in typed_compounds:
                self.writenl(".. toctree::")
                self.writenl("   :maxdepth: 1")
                self.writenl()

                fq_name = self.storage.relations.get_fully_qualified_path(compound, till=common_ns)
                if not self.storage.has_name_collision(compound):
                    self.writenl("   {}", self.get_file_name(compound))
                else:
                    self.writenl("   {} <{}>", fq_name, self.get_file_name(compound))

                self.writenl()

                self.writenl("- :ref:`{} <{}>`", fq_name, self.get_ref(compound))
                self.writenl()

        # for parent_cls in compounds.get_parents():
        #     for compound in self.storage.get_orphan_compounds(parent_cls):
        #         self._render_l1_children(compound, "")

        # for orphan_cls in get_child_compounds():
        #     for compound in self.storage.get_orphan_compounds(orphan_cls):
        #         self._render_list_item(compound)

        # self.writenl()

    # def _render_list_item(self, compound: "BaseCompound") -> None:
    #     self.writenl("{}- ``{}`` :ref:`{} <{}>`", compound.ctype, compound.name, self.get_ref(compound))

    # def _render_l1_children(self, root_compound: compounds.Directory) -> None:
    #     self._render_list_item(root_compound, indent)
    #     indent += self.default_list_indent

    #     for parent_cls in compounds.get_parents():
    #         for folder in self.storage.relations.get_children(root_compound, parent_cls):
    #             self._render_l1_children(folder, indent)

    #     for orphan_cls in get_child_compounds():
    #         for compound in self.storage.relations.get_children(root_compound, orphan_cls):
    #             self._render_list_item(compound, indent)

    def _get_common_namespace(self) -> Optional[Sequence["BaseCompound"]]:
        result: List["BaseCompound"] = list(self.storage.get_orphan_compounds(compounds.Namespace))
        if len(result) != 1:  # No namespaces or more then 1 "root" namespaces.
            return None

        while True:
            child_namespaces = self.storage.relations.get_children(result[-1], compounds.Namespace)
            if len(child_namespaces) != 1:
                break

            result.append(child_namespaces[0])

        return result if result else None
