
from typing import TYPE_CHECKING

from inhale.locale import _
from inhale.storage import compounds

from ._base import BaseRenderer, get_child_compounds

if TYPE_CHECKING:
    from inhale.storage.compounds import BaseCompound


class HierarchyRenderer(BaseRenderer):
    index_priority = 1
    index_filename = "_hierarchy"
    index_refid = "hierarchy"

    def do_render(self) -> None:
        self.h1(_("Hierarchy"))
        self.writenl()

        for parent_cls in compounds.get_parents():
            for compound in self.storage.get_orphan_compounds(parent_cls):
                self._render_l1_children(compound, "")

        for orphan_cls in get_child_compounds():
            for compound in self.storage.get_orphan_compounds(orphan_cls):
                self._render_list_item(compound, indent="")

        for cls in compounds.get_all_types():
            if self.storage.get_compounds(cls):
                self.writenl()
                break

    def _render_list_item(self, compound: "BaseCompound", indent: str) -> None:
        self.writenl("{}- ``{}`` :ref:`{} <{}>`", indent, compound.ctype, compound.name, self.get_ref(compound))

    def _render_l1_children(self, root_compound: compounds.Directory, indent: str) -> None:
        self._render_list_item(root_compound, indent)
        indent += self.default_list_indent

        for parent_cls in compounds.get_parents():
            for folder in self.storage.relations.get_children(root_compound, parent_cls):
                self._render_l1_children(folder, indent)

        for orphan_cls in get_child_compounds():
            for compound in self.storage.relations.get_children(root_compound, orphan_cls):
                self._render_list_item(compound, indent)
