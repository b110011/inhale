from inhale.locale import _
from inhale.storage import compounds

from ._base import BaseRenderer


class NamespaceRenderer(BaseRenderer):
    def do_render(self, ns: compounds.Namespace) -> None:
        self.h1("{} {}", _(ns.type), ns.name)
        self.writenl()

        self.render_content(ns, use_fq_names=False)
