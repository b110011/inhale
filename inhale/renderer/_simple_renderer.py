from typing import TYPE_CHECKING

from inhale.locale import _
from inhale.storage import compounds

from ._base import BaseRenderer, template

if TYPE_CHECKING:
    from inhale.storage.compounds import BaseCompound


class SimpleRenderer(BaseRenderer):
    def do_render(self, compound: "BaseCompound") -> None:
        self.h1("{}{} {}", template(compound), _(compound.type), compound.name)
        self.writenl()

        for file in self.storage.relations.get_parents(compound, compounds.File):
            self.writenl("- {} :ref:`{} <{}>`", _("Defined in"), file.path, file.refid)
            self.writenl()

        self.h2("{} {}", _(compound.type), _("Documentation"))
        self.writenl()

        self.render_doxygen_directive(compound)
