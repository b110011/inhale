from typing import TYPE_CHECKING

from inhale.locale import _
from inhale.storage import compounds

from ._base import BaseRenderer

if TYPE_CHECKING:
    from inhale.storage.compounds import BaseCompound


class FileSystemRenderer(BaseRenderer):
    index_priority = 2
    index_filename = "_filesystem"
    index_refid = "filesystem"

    def do_render(self) -> None:
        self.h1(_("Filesystem"))
        self.writenl()

        for root_folder in self.storage.get_orphan_compounds(compounds.Directory):
            self._render_l1_children(root_folder, "")

        for root_file in self.storage.get_orphan_compounds(compounds.File):
            self._render_list_item(root_file, indent="")

        total_count = len(self.storage.get_compounds(compounds.Directory))
        total_count += len(self.storage.get_compounds(compounds.File))

        if total_count:
            self.writenl()

    def _render_list_item(self, compound: "BaseCompound", indent: str) -> None:
        self.writenl("{}- :ref:`{} <{}>`", indent, compound.name, self.get_ref(compound))

    def _render_l1_children(self, root_folder: compounds.Directory, indent: str) -> None:
        self._render_list_item(root_folder, indent)
        indent += self.default_list_indent

        # TODO(b110011): Can doxygen generate empty folder?
        for folder in self.storage.relations.get_children(root_folder, compounds.Directory):
            self._render_l1_children(folder, indent)

        for file in self.storage.relations.get_children(root_folder, compounds.File):
            self._render_list_item(file, indent)
