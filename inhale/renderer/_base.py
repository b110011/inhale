__all__ = [
    "BaseRenderer",
    "check_renderers_compliance",
    "get_child_compounds",
    "get_index_renderers",
    "get_type_plural",
    "template"
]

import base64
import hashlib
import platform
from abc import ABC, abstractmethod
from functools import cache
from pathlib import Path
from typing import Sequence, TextIO, Type, Tuple, Dict, List, Optional, Union, TYPE_CHECKING, overload, Generator

from sortedcontainers import SortedDict
from sphinx.errors import ExtensionError

from inhale.config import Config, OutputFilenameFormat
from inhale.locale import _, ngettext
from inhale.storage import compounds

if TYPE_CHECKING:
    from io import StringIO

    from inhale.config import Config
    from inhale.storage import Storage
    from inhale.storage.compounds import BaseCompound


def _get_refid_hash(refid: str) -> str:
    md5_hash = hashlib.md5(refid.encode()).digest()
    return base64.urlsafe_b64encode(md5_hash).decode().rstrip("=")


class BaseRenderer(ABC):
    default_list_indent: str = "  "

    index_priority: Optional[int] = None
    index_filename: Optional[str] = None
    index_refid: Optional[str] = None

    def __init__(self, storage: "Storage", config: "Config") -> None:
        self._storage = storage
        self._config = config
        self._stream: Optional[TextIO] = None

        self._project = self._config.project.replace("-", "")

        self._filename_format = self._config.inhale_output_filename_format
        if self._filename_format == OutputFilenameFormat.AUTO:
            if platform.system() == "Windows":
                self._filename_format = OutputFilenameFormat.MD5
            else:
                self._filename_format = OutputFilenameFormat.REFID

    @property
    def config(self) -> "Config":
        return self._config

    @property
    def storage(self) -> "Storage":
        return self._storage

    def render(self, compound: Optional["BaseCompound"] = None, *, stream: Optional["StringIO"] = None) -> None:
        if stream is None:
            self._stream = open(self.get_file_path(compound), mode="w")
            # TODO: self._stream = self.get_file_path(compound).open(mode=w)
        else:
            self._stream = stream

        try:
            self.writenl(":orphan:")
            self.writenl()
            self.writenl(".. {}:", self.get_ref(compound))
            self.writenl()

            if compound:
                self.do_render(compound)
            else:
                self.do_render()
        finally:
            if not stream:
                self._stream.close()

            self._stream = None

    @overload
    def do_render(self) -> None:
        ...

    @overload
    def do_render(self, compound: "BaseCompound") -> None:
        ...

    @abstractmethod
    def do_render(self, *args) -> None:
        ...

    # text writer

    def writenl(self, msg:Optional[str] = None, *args: Tuple, **kwargs: Dict) -> None:
        if msg:
            self._stream.write(msg.format(*args, **kwargs))
        self._stream.write("\n")

    # refs

    def get_file_name(self, compound: Optional["BaseCompound"] = None) -> Path:
        if compound:
            if self._filename_format == OutputFilenameFormat.REFID:
                filename = compound.refid
            else:
                filename = _get_refid_hash(compound.refid)
        else:
            filename = self.index_filename

        assert filename
        return Path(filename).with_suffix(".rst")

    def get_file_path(self, compound: Optional["BaseCompound"] = None) -> Path:
        return self.get_file_name(compound)
        # TODO: return (self._config.output_dir / self.get_file_name(compound))

    def get_ref(self, compound: Optional["BaseCompound"] = None) -> str:
        if compound:
            refid = compound.refid
        else:
            refid = self.index_refid

        assert refid
        return f"inhale__{self._project}__{refid}"

    # headings

    def _heading(self, msg: str, ch: str, *args: Tuple, **kwargs: Dict) -> None:
        s = msg.format(*args, **kwargs)

        self.writenl(s)
        self.writenl(ch * len(s))

    def h1(self, msg: str, *args: Tuple, **kwargs: Dict) -> None:
        self._heading(msg, "#", *args, **kwargs)

    def h2(self, msg: str, *args: Tuple, **kwargs: Dict) -> None:
        self._heading(msg, "*", *args, **kwargs)

    def h3(self, msg: str, *args: Tuple, **kwargs: Dict) -> None:
        self._heading(msg, "=", *args, **kwargs)

    # utils

    def render_doxygen_directive(self, compound: "BaseCompound") -> None:
        """
        +-------------+--------------------+------+
        | Input Kind  | Output Directive   | Used |
        +=============+====================+======+
        |             | "autodoxygenindex" | No   |
        +-------------+--------------------+------+
        |             | "autodoxygenfile"  | No   |
        +-------------+--------------------+------+
        | "class"     | "doxygenclass"     | Yes  |
        +-------------+--------------------+------+
        | "class"     | "doxygenclass"     | Yes  |
        +-------------+--------------------+------+
        | "concept"   | "doxygenconcept"   | Yes  |
        +-------------+--------------------+------+
        | "define"    | "doxygendefine"    | Yes  |
        +-------------+--------------------+------+
        | "enum"      | "doxygenenum"      | Yes  |
        +-------------+--------------------+------+
        |             | "doxygenenumvalue" | No   |
        +-------------+--------------------+------+
        | "file"      | "doxygenfile"      | No   |
        +-------------+--------------------+------+
        | "function"  | "doxygenfunction"  | Yes  |
        +-------------+--------------------+------+
        | "group"     | "doxygengroup"     | No   |
        +-------------+--------------------+------+
        |             | "doxygenindex"     | No   |
        +-------------+--------------------+------+
        | "namespace" | "doxygennamespace" | Yes  |
        +-------------+--------------------+------+
        |             | "doxygenpage"      | No   |
        +-------------+--------------------+------+
        | "struct"    | "doxygenstruct"    | Yes  |
        +-------------+--------------------+------+
        | "typedef"   | "doxygentypedef"   | Yes  |
        +-------------+--------------------+------+
        | "union"     | "doxygenunion"     | Yes  |
        +-------------+--------------------+------+
        | "variable"  | "doxygenvariable"  | Yes  |
        +-------------+--------------------+------+
        """
        directive = f"doxygen{compound.ctype}"

        self.writenl(".. {}:: {}", directive, self.storage.relations.get_fully_qualified_path(compound))
        self.writenl("   :project: {}", self.config.project)

        for arg in self.config.inhale_directives_args.get(directive, ()):
            self.writenl("   :{}:", arg)

        self.writenl()

    def render_content(self, compound: "BaseCompound", *, use_fq_names: bool) -> None:
        if not self.storage.relations.has_children(compound):
            return

        self.h2(_("Content"))
        self.writenl()

        for child_compound_cls in compounds.get_children():
            children: Sequence[BaseCompound] = self.storage.relations.get_children(compound, child_compound_cls)
            if not children:
                continue

            self.h3(get_type_plural(child_compound_cls, len(children)))
            self.writenl()

            for child in children:
                if use_fq_names:
                    name = self.storage.relations.get_fully_qualified_path(child)
                else:
                    name = child.name

                self.writenl("- :ref:`{} <{}>`", name, self.get_ref(child))

            self.writenl()

    def render_parent_link(self, link_text: str, link_ref: str) -> None:
        self.writenl(".. |inhale_back_symbol| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS")
        self.writenl()
        self.writenl("|inhale_back_symbol| :ref:`{} <{}>`", link_text, link_ref)
        self.writenl()

    def render_parent_dir(self, compound: "BaseCompound", msg: str) -> None:
        parent_dirs = self.storage.relations.get_parents(compound, compounds.Directory)
        if not parent_dirs:
            return

        assert len(parent_dirs) == 1
        parent_dir: "compounds.Directory" = next(iter(parent_dirs))

        self.render_parent_link(msg, self.get_ref(parent_dir))


_L10N_PlURALS = {
    _("Class"): _("Classes"),
    _("Concept"): _("Concepts"),
    _("Define"): _("Defines"),
    _("Enum"): _("Enums"),
    _("Function"): _("Functions"),
    _("Group"): None,
    _("Namespace"): _("Namespaces"),
    _("Page"): None,
    _("Struct"): _("Structs"),
    _("Typedef"): _("Typedefs"),
    _("Union"): _("Unions"),
    _("Variable"): _("Variables"),
}


def get_index_renderers() -> Generator[BaseRenderer, None, None]:
    for subcls in BaseRenderer.__subclasses__():
        if subcls.index_priority is not None:
            yield subcls


def _check_index_attrs() -> None:
    cls = BaseRenderer

    index_attrs = tuple(filter(lambda a: a.startswith("index_"), dir(cls)))
    valid_count = (0, len(index_attrs))

    for subcls in cls.__subclasses__():
        values: Dict[str, bool] = SortedDict()
        count: int = 0

        for index_attr in index_attrs:
            value = getattr(subcls, index_attr) is not None
            values[index_attr] = value

            count += value

        if count not in valid_count:
            raise ExtensionError(
                "{} is missing {} attributes!".format(
                    subcls.__name__, ",".join(filter(lambda v: not v, values.values()))
                )
            )


def _check_index_priority() -> None:
    indices: Dict[int, Type] = {}
    for subcls in get_index_renderers():
        priority = subcls.index_priority

        prev_subcls = indices.get(priority)
        if prev_subcls:
            raise ExtensionError(f"{prev_subcls.__name__} and {subcls.__name__} have index priority collision!")

        indices[priority] = subcls


def check_renderers_compliance() -> None:
    _check_index_attrs()
    _check_index_priority()


@cache
def get_child_compounds() -> Sequence[Type["BaseCompound"]]:
    result: List[Type["BaseCompound"]] = []

    for child_compound_cls in compounds.get_children():
        if child_compound_cls not in compounds.get_parents():
            result.append(child_compound_cls)

    return tuple(result)


def get_type_plural(value_or_cls: Union["BaseCompound", Type["BaseCompound"]], n: int) -> str:
    compound_type: str = value_or_cls.type
    return ngettext(compound_type, _L10N_PlURALS[compound_type], n)


def template(compound: "BaseCompound") -> str:
    if hasattr(compound, "tparams") and len(compound.tparams) != 0:
        return _("Template")

    return ""
